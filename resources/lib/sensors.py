def r_button():
    """
    @summary: gets the state of the R button
    @return: 1 if the button is pressed, 0 otherwise
    @rtype: bool
    """

    pass

def l_button():
    """
    @summary: gets the state of the L button
    @return: 1 if the button is pressed, 0 otherwise
    @rtype: bool
    """

    pass

def wait_for_r():
    """
    @summary: waits until the R button is pressed
    """

    pass

def wait_for_l():
    """
    @summary: waits until the L button is pressed
    """

    pass

def r_clicked():
    """
    @summary: waits for the R button to be clicked and released
    """

    pass

def l_clicked():
    """
    @summary: waits for the L button to be clicked and released
    """

    pass

def accel_x():
    """
    @summary: gets the value of the accelerometer in its x direction
    @return: the accelerometer value
    @rtype: number
    """

    pass

def accel_y():
    """
    @summary: gets the value of the accelerometer in its y direction
    @return: the accelerometer value
    @rtype: number
    """

    pass

def accel_z():
    """
    @summary: gets the value of the accelerometer in its z direction
    @return: the accelerometer value
    @rtype: number
    """

    pass

def analog(port):
    """
    @summary: gets the sensor value for an analog sensor
    @param port: the port of the sensor
    @type port: number
    @return: the sensor value between 0 and 1023
    @rtype: number
    """

    pass

def analog_et(port):
    """
    @summary: gets the sensor value for an ET sensor
    @param port: the port of the sensor
    @type port: number
    @return: the sensor value between 0 and 1023
    @rtype: number
    """

    pass

def analog8(port):
    """
    @summary: gets the sensor value for an analog sensor as an 8-bit integer
    @param port: the port of the sensor
    @type port: number
    @return: the sensor value between 0 and 255
    @rtype: number
    """

    pass

def digital(port):
    """
    @summary: gets the sensor value for a digital sensor
    @param port: the port of the sensor
    @type port: number
    @return: if the sensor is activated
    @rtype: bool
    """

    pass

def power_level():
    """
    @summary: gets the controller power level
    @return: the power level between 0 and 1000
    @rtype: number
    """

    pass

def gyro_x():
    """
    @summary: gets the value of the gyroscope in its x direction
    @return: the gyroscope value
    @rtype: number
    """

    pass

def gyro_y():
    """
    @summary: gets the value of the gyroscope in its y direction
    @return: the gyroscope value
    @rtype: number
    """

    pass

def gyro_z():
    """
    @summary: gets the value of the gyroscope in its z direction
    @return: the gyroscope value
    @rtype: number
    """

    pass

def magneto_x():
    """
    @summary: gets the value of the magnetometer in its x direction
    @return: the magnetometer value
    @rtype: number
    """

    pass

def magneto_y():
    """
    @summary: gets the value of the magnetometer in its y direction
    @return: the magnetometer value
    @rtype: number
    """

    pass

def magneto_z():
    """
    @summary: gets the value of the magnetometer in its z direction
    @return: the magnetometer value
    @rtype: number
    """

    pass

def compass():
    """
    @summary: gets the compass heading of the bot. Magnetometers have to be calibrated in order for this function to work
    @return: the compass value in deg
    @rtype: number
    """

    pass

def compass_x():
    """
    @summary: gets the compass heading while the wallaby is lying flat on the x axis
    @return: the compass heading in deg
    @rtype: number
    """

    pass

def compass_y():
    """
    @summary: gets the compass heading while the wallaby is lying flat on the y axis
    @return: the compass heading in deg
    @rtype: number
    """

    pass

def compass_z():
    """
    @summary: gets the compass heading while the wallaby is lying flat on the z axis
    @return: the compass heading in deg
    @rtype: number
    """

    pass

def get_magneto_calibration():
    """
    @summary: reads the magnetometer calibration from the file
    """

    pass

def get_magneto_calibration_x():
    """
    @summary: gets the magnetometer calibration value for the x axis
    @return: the calibration value
    @rtype: number
    """

    pass

def get_magneto_calibration_y():
    """
    @summary: gets the magnetometer calibration value for the y axis
    @return: the calibration value
    @rtype: number
    """

    pass

def get_magneto_calibration_z():
    """
    @summary: gets the magnetometer calibration value for the z axis
    @return: the calibration value
    @rtype: number
    """

    pass

def calibrate_magneto():
    """
    @summary: calibrates the magnetometer. This process only has to be done once and will be saved to a file after completion; however, when the wallaby is moved to another location significantly(eg. other continent), it should be recalibrated
    """

    pass
