def create_connect():
    """
    @author: Lukas Rysavy
    @summary: creates a new Create object
    @return: the Create object
    @rtype: Create
    """
    
    pass

class Create(object):

    def __init__(self, port):
        """
        @author: Manuel Reinsperger
        @summary: connect to Create
        @Sends: 128 Start
        """
    
        pass

    def send_command_ASCII(self, command):
        """
        @author: Manuel Reinsperger
        @summary: take an ASCII string, split it by whitespace and send it via sendCommandRaw
        @args: command (string): command to send
        """
    
        pass

    def send_command_raw(self, command):
        """
        @author: Manuel Reinsperger
        @summary: take string interpreted as a byte array and sends it to create
        @args: command (char): command to send
        @return: 1 success, -1 not connected, -2 connection lost
        @rtype: int
        """
    
        pass

    def stop_all(self):
        """
        @author: Manuel Reinsperger
        @summary: stop the create
        @aends: 145 0 0 0 0 stop both motors
        """
    
        pass

    def drive_distance(self, speed, dist):
        """
        @author: Manuel Reinsperger
        @summary: drive the distance with speed, must be both positive or negative
        @args: speed (int): average speed in mm/s
        @sends: 152         Define of external script
        """
    
        pass

    def drive_distance_duration(self, speed, dist):
        """
        @author: Manuel Reinsperger
        @summary: calculate the approximate time that driving will take
        @args: speed (int): average speed in mm/s
        @return: duration in seconds
        @rtype: int
        """
    
        pass

    def drive_distance_wait(self, speed, dist):
        """
        @author: Manuel Reinsperger
        @summary: use the driveDistance and return after finished driving
        @args: speed (int): average speed in mm/s
        """
    
        pass

    def spin_angle(self, speed, angle):
        """
        @author: Manuel Reinsperger
        @summary: turn in place for a certain angle
        @args: speed (int): average speed in mm/s
        @sends: 152             Define of external script
        """
    
        pass

    def spin_angle_duration(self, speed, angle):
        """
        @author: Manuel Reinsperger
        @summary: calculate the approximate time that turning will take
        @args: speed (int): average speed in mm/s
        @return: duration in seconds
        @rtype: int
        """
    
        pass

    def spin_angle_wait(self, speed, angle):
        """
        @author: Manuel Reinsperger
        @summary: use the spinAngle and return after finished turning
        @args: speed (int): average speed in mm/s
        """
    
        pass

    def poll_sensor(self, sensor):
        """
        @author: Manuel Reinsperger
        @summary: poll create for sensor value
        @args: sensor (int): number of sensor, see documentation for more
        @return: sensor value
        @rtype: int
        """
    
        pass

    def set_mode(self, mode):
        """
        @author: Manuel Reinsperger
        @summary: sets mode to safe or full
        @args: mode (int): mode type, 0 for safe, 1 for full
        """
    
        pass

    def drive_direct(self, rightmotor, leftmotor):
        """
        @author: Manuel Reinsperger
        @summary: start driving with motor speeds
        @args: speed (int): speed in mm/s
        @sends: 145   Drive Direct command
        """
    
        pass

    def drive_circle(self, speed, radius):
        """
        @author: Manuel Reinsperger
        @summary: start driving with motor speeds
        @args: speed (int) in mm/s
        @sends: 137   Drive Direct command
        """

        pass
