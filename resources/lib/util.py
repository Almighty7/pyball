def beep():
    """
    @summary: lets the controller beep
    """

    pass

def set_digital_output(port, value):
    """
    @summary: sets a digital port to output the given value
    @param port: the port which should output the value
    @type port: number
    @param value: the value to set the port to
    @type value: bool
    """

    pass

def set_digital_input(port):
    """
    @summary: sets a digital port to receive input
    @param port: the port which should receive input
    @type port: number
    """

    pass

def wait_for_light(port):
    """
    @summary: first calibrates the light sensor, then waits until the light is turned on
    @param port: the port on which the sensor is connected
    @type port: number
    """

    pass

def shut_down_in(seconds):
    """
    @summary: lets the bot stop all motors and sensors after the given time
    @param seconds: the time to stop after in seconds
    @type seconds: number
    """

    pass

def shut_down():
    """
    @summary: lets the controller completely shut down everything(stop motors, servos,...)
    """

    pass

def register_create(create):
    """
    @summary: registers a create connection in order for it to be shut down in shut_down_in
    @param create: the create object to register
    @type create: Create
    """

    pass
