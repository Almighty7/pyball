'''
This is the file where you should place your constants 
'''

# section angles
ANGLE_FULL_CIRCLE = 360
ANGLE_HALF_CIRCLE = 180
# endsection angles

# section time
TIME_GAME_DURATION = 119
# endsection time

# section distance
# endsection distance

# section speed
# endsection speed

# section camera
# endsection camera

# section motor
# endsection motor

# section servo
# endsection servo

# section other
# endsection other
