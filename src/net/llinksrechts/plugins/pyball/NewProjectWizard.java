package net.llinksrechts.plugins.pyball;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.python.pydev.shared_core.callbacks.ICallback;
import org.python.pydev.ui.wizards.project.PythonProjectWizard;

public class NewProjectWizard extends PythonProjectWizard {
	@Override
	public boolean performFinish() {
		boolean ret = super.performFinish();

		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
				.getProjects();

		IProject newest = null;
		for (IProject project : projects)
			if (newest == null
					|| newest.getLocalTimeStamp() < project.getLocalTimeStamp())
				newest = project;

		// System.out.println(newest.getName());

		return ret;
	}

	@Override
	protected void createAndConfigProject(IProject newProjectHandle,
			IProjectDescription description, String projectType,
			String projectInterpreter, IProgressMonitor monitor,
			Object... additionalArgsToConfigProject) throws CoreException {
		ICallback<List<org.eclipse.core.resources.IContainer>, IProject> getSourceFolderHandlesCallback = this.getSourceFolderHandlesCallback;
		ICallback<List<org.eclipse.core.runtime.IPath>, IProject> getExistingSourceFolderHandlesCallback = this.getExistingSourceFolderHandlesCallback;

		BotStructureConfigHelpers.createBotballProject(description,
				newProjectHandle, monitor, projectType, projectInterpreter,
				getSourceFolderHandlesCallback, null,
				getExistingSourceFolderHandlesCallback, null);
		Scanner sc = new Scanner(getClass().getResourceAsStream(
				"/resources/templates/constants.py"));
		sc.useDelimiter("\\A");
		String content = sc.next();
		sc.close();
		try {
			FileWriter w = new FileWriter(new File(
					newProjectHandle.getLocation() + "/constants.py"));
			w.write(content);
			w.close();
		} catch (IOException e) {
		}

		sc = new Scanner(getClass().getResourceAsStream(
				"/resources/templates/__main__.py"));
		sc.useDelimiter("\\A");
		content = sc.next();
		sc.close();
		try {
			FileWriter w = new FileWriter(new File(
					newProjectHandle.getLocation() + "/__main__.py"));
			w.write(content);
			w.close();
		} catch (IOException e) {
		}

		createLibrary();
	}

	public static void createLibrary() {
		Scanner sc;
		String content;
		String home = System.getProperty("user.home");
		File f = new File(home + "/.pyball");
		if (!f.isDirectory()) {
			f.mkdir();
		}
		String[] files = { "motors.py", "sensors.py", "util.py", "servos.py", "camera.py", "create.py" };
		for (String file : files) {
			sc = new Scanner(NewProjectWizard.class.getResourceAsStream("/resources/lib/" + file));
			sc.useDelimiter("\\A");
			content = sc.next();
			sc.close();
			try {
				FileWriter fw = new FileWriter(home + "/.pyball/" + file, false);
				fw.write(content);
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
