package net.llinksrechts.plugins.pyball.dialogs;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class DeviceSelectionDialog extends Dialog {

	private List outputs = null;
	private String returnString = null;
	private Text targetTb = null;
	private Button refreshButton = null;
	private boolean reloaded = false;

	public DeviceSelectionDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		outputs = new List(container, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		outputs.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, true, 1,
				1));
		outputs.add("Other(Specify below)");
		outputs.add("----------------------------------------------");
		outputs.add("");
		outputs.add("");
		outputs.add("");

		refreshButton = new Button(container, SWT.CENTER);
		refreshButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
				true, 1, 1));
		refreshButton.setText("Loading...");
		refreshButton.setEnabled(false);
		refreshButton.addMouseListener(new MouseListener() {

			public void mouseUp(MouseEvent e) {
				refreshButton.setEnabled(false);
				refreshButton.setText("Loading...");
				reloadSources();
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		targetTb = new Text(container, SWT.CENTER);
		targetTb.setLayoutData(new GridData(SWT.CENTER, SWT.BOTTOM, true, true,
				1, 1));
		targetTb.setMessage("Enter target address here");

		container.addPaintListener(new PaintListener() {

			public void paintControl(PaintEvent e) {
				firstReload();
			}
		});

		return container;
	}

	private void firstReload() {
		if (reloaded)
			return;
		reloadSources();
		reloaded = true;
	}

	public String openDialog() {
		if (super.open() == 1)
			return null;
		return this.returnString;
	}

	@Override
	protected void okPressed() {
		String[] selected = outputs.getSelection();
		if (outputs.getSelectionIndex() == 0) {
			returnString = null;
		} else {
			if (selected.length == 0)
				returnString = null;
			else
				returnString = selected[0].split("-")[1].trim();
		}
		if (returnString == null && !this.targetTb.getText().equals(""))
			returnString = this.targetTb.getText();
		super.okPressed();
	}

	private void reloadSources() {
		outputs.removeAll();
		outputs.add("Other(Specify below)");

		Thread t = new Thread(new Runnable() {

			public void run() {
				ArrayList<String> content = new ArrayList<String>();
				try {
					if (InetAddress.getByName("192.168.124.1").isReachable(3000)) {
						content.add("wallaby - 192.168.124.1");
						Display.getDefault().asyncExec(
								new UpdateRunnable(content, false, outputs,
										refreshButton, true));
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
						content.clear();
					}
					if (InetAddress.getByName("192.168.125.1").isReachable(3000)) {
						content.add("wallaby - 192.168.125.1");
						Display.getDefault().asyncExec(
								new UpdateRunnable(content, false, outputs,
										refreshButton, true));
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
						content.clear();
					}
				} catch (IOException e1) {
				}
				boolean error = false;
				try {
					Process p = Runtime.getRuntime().exec("getDevices");
					p.waitFor();
					BufferedReader b = new BufferedReader(
							new InputStreamReader(p.getInputStream()));
					String line = "";

					while ((line = b.readLine()) != null) {
						content.add(line);
					}
				} catch (Exception e) {
					error = true;
				}
				Display.getDefault().asyncExec(
						new UpdateRunnable(content, error, outputs,
								refreshButton));
			}
		});
		t.start();
	}
}

class UpdateRunnable implements Runnable {

	private ArrayList<String> content;
	private boolean error;
	private List outputs;
	private Button refreshButton;
	private boolean notFinished;

	public UpdateRunnable(ArrayList<String> content, boolean error,
			List outputs, Button refreshButton) {
		this(content, error, outputs, refreshButton, false);
	}

	public UpdateRunnable(ArrayList<String> content, boolean error,
			List outputs, Button refreshButton, boolean notFinished) {
		this.content = content;
		this.error = error;
		this.outputs = outputs;
		this.refreshButton = refreshButton;
		this.notFinished = notFinished;
	}

	public void run() {
		for (String output : content)
			outputs.add(output);
		if (!notFinished) {
			refreshButton.setText("Refresh");
			if (error)
				refreshButton.setText("Error");
			refreshButton.setEnabled(true);
		}
	}
}
