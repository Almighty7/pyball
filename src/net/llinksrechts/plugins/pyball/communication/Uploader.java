package net.llinksrechts.plugins.pyball.communication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

import net.llinksrechts.plugins.pyball.NewProjectWizard;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.rauschig.jarchivelib.ArchiveFormat;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;
import org.rauschig.jarchivelib.CompressionType;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class Uploader {

	static {
		NewProjectWizard.createLibrary();
	}

	public static String target = null;

	public static void compile(Object o, String path,
			MessageConsoleStream consoleStream) throws Exception {
		File project = new File(path);
		if (path == null)
			return;
		Scanner sc = new Scanner(o.getClass().getResourceAsStream(
				"/resources/beforeCompile.sh"));
		sc.useDelimiter("\\A");
		String scriptBefore = String.format(sc.next(), project.getName());
		sc.close();
		sc = new Scanner(o.getClass().getResourceAsStream(
				"/resources/afterCompile.sh"));
		sc.useDelimiter("\\A");
		String scriptAfter = String.format(sc.next(), project.getName());
		sc.close();
		JSch jsch = new JSch();
		String home = System.getProperty("user.home");
		jsch.addIdentity(home + "/.pyball/sshkey");
		Session session = jsch.getSession("root", target);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.setTimeout(25000);

		Channel channel = null;
		try {
			session.connect();
			try {
				channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(scriptBefore);
				channel.setInputStream(null);
				((ChannelExec) channel).setErrStream(System.err);
				channel.connect();
				while (!channel.isClosed()) {
				}
			} catch (Exception e) {
				throw e;
			} finally {
				if (channel != null)
					channel.disconnect();
			}

			File archive = createArchive(path, consoleStream, project.getName());
			if (archive == null) {
				throw new Exception("Could not create archive");
			}
			try {
				channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand("mkdir /tmp/pyCopy/"
						+ project.getName());
				channel.setInputStream(null);
				((ChannelExec) channel).setErrStream(System.err);
				channel.connect();
				while (!channel.isClosed()) {
				}
			} catch (Exception e) {
				throw e;
			} finally {
				if (channel != null)
					channel.disconnect();
			}
			uploadFile(session, archive, "/tmp/pyCopy/" + project.getName(),
					consoleStream);
			// Thread.sleep(200);
			archive.delete();

			consoleStream.write("Compiling...\n");
			try {
				channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(scriptAfter);
				channel.setInputStream(null);
				((ChannelExec) channel).setErrStream(System.err);
				channel.connect();
				while (!channel.isClosed()) {
				}
			} catch (JSchException e) {
				session = jsch.getSession("root", target);
				session.setConfig(config);
				session.setTimeout(25000);
				channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(scriptAfter);
				channel.setInputStream(null);
				((ChannelExec) channel).setErrStream(System.err);
				channel.connect();
				while (!channel.isClosed()) {
				}
			} catch (Exception e) {
				throw e;
			} finally {
				if (channel != null)
					channel.disconnect();
			}

			consoleStream.write("Finished compiling\n");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			session.disconnect();
		}
	}

	private static void uploadFolder(Session session, String path,
			String serverPath, MessageConsoleStream consoleStream)
			throws Exception {
		Channel channel = null;
		try {
			channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand("mkdir " + serverPath);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
			channel.connect();
			while (!channel.isClosed()) {
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (channel != null)
				channel.disconnect();
		}
		File dir = new File(path);
		for (File f : dir.listFiles()) {
			if (f.isDirectory())
				uploadFolder(session, f.getAbsolutePath(),
						serverPath + "/" + f.getName(), consoleStream);
			else if (f.getName().endsWith(".py")) {
				uploadFile(session, f, serverPath, consoleStream);
			}
		}
	}

	private static void uploadFile(Session session, File file,
			String serverPath, MessageConsoleStream consoleStream)
			throws JSchException, FileNotFoundException {
		Channel channel = session.openChannel("exec");
		channel.connect();
		byte[] data = new byte[(int) file.length()];
		try {
			consoleStream.write(String.format("Uploading %s...\n",
					file.getName()));
			Path path = Paths.get(file.getAbsolutePath());
			data = Files.readAllBytes(path);
		} catch (IOException e) {
		}
		StringBuilder sb = new StringBuilder(data.length * 2);
		for (int i : data) {
			if (i < 0)
				i += 256;
			sb.append(" ");
			sb.append(i);
		}
		String command = String.format(
				"echo -ne \"$(printf '\\x%s'%s 2> /dev/null)\" > %s/%s", "%x",
				sb.toString(), serverPath, file.getName());
		((ChannelExec) channel).setCommand(command);
		channel.setInputStream(null);
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
		}
		((ChannelExec) channel).setErrStream(System.err);
		channel.connect();
		while (!channel.isClosed()) {
		}
		try {
			Thread.sleep(file.length() / 4);
		} catch (InterruptedException e) {
		}
		channel.disconnect();
		try {
			consoleStream.write(String.format("Uploaded %s\n", file.getName()));
		} catch (IOException ex) {
		}
	}

	public static void run(Object o, String path, MessageConsole console)
			throws Exception {
		MessageConsoleStream consoleStream = console.newMessageStream();
		compile(o, path, consoleStream);
		File project = new File(path);
		if (path == null)
			return;
		Scanner sc = new Scanner(o.getClass().getResourceAsStream(
				"/resources/run.sh"));
		sc.useDelimiter("\\A");
		String scriptRun = String.format(sc.next(), project.getName());
		sc.close();

		JSch jsch = new JSch();
		String home = System.getProperty("user.home");
		jsch.addIdentity(home + "/.pyball/sshkey");
		Session session = jsch.getSession("root", target);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		Channel channel = null;
		try {
			session.connect();
			console.clearConsole();
			consoleStream.write(String.format("Running %s:\n\n",
					project.getName()));
			channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(scriptRun);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
			((ChannelExec) channel).setOutputStream(consoleStream);
			channel.connect();
			while (!channel.isClosed()) {
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (channel != null)
				channel.disconnect();
			session.disconnect();
			console.newMessageStream().write("Finished");
		}
	}

	public static String getProjectPath() {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		String project = null;
		IEditorPart editorPart = window.getActivePage().getActiveEditor();

		if (editorPart != null) {
			IFileEditorInput input = (IFileEditorInput) editorPart
					.getEditorInput();
			IFile file = input.getFile();
			IProject activeProject = file.getProject();
			project = activeProject.getLocation().toString();
		}
		return project;
	}

	public static MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

	public static boolean ensureKeyExists() {
		return ensureKeyExists(false);
	}

	public static boolean ensureKeyExists(boolean reselectKey) {
		String home = System.getProperty("user.home");
		File key = new File(home + "/.pyball/sshkey");
		if (key.exists() && !reselectKey) {
			return true;
		}
		FileDialog dialog = new FileDialog(new Shell(), SWT.OPEN);
		dialog.setText("Select ssh key");
		String result = dialog.open();
		if (result == null) {
			return false;
		}
		File newKey = new File(result);
		try {
			if (reselectKey) {
				key.delete();
			}
			try (FileChannel in = new FileInputStream(newKey).getChannel();
					FileChannel out = new FileOutputStream(key).getChannel()) {
				out.transferFrom(in, 0, in.size());
			}
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	private static File createArchive(String path, MessageConsoleStream stream,
			String projectName) {
		try {
			String archiveName = projectName + ".tar.gz";
			stream.write(String.format("Compressing project to %s...\n",
					archiveName));
			File destination = new File(System.getProperty("java.io.tmpdir"));
			File source = new File(path);

			Archiver archiver = ArchiverFactory.createArchiver(
					ArchiveFormat.TAR, CompressionType.GZIP);
			File archive = archiver.create(archiveName, destination, source);
			return archive;
		} catch (IOException ex) {
			try {
				stream.write(String.format(
						"Error: could not compress project to %s\n",
						System.getProperty("java.io.tmpdir")));
			} catch (IOException e) {
			}
			return null;
		}
	}
}
