package net.llinksrechts.plugins.pyball.handlers;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class RefactorHandler extends AbstractHandler {

	public static String VALID_NUMBER = "0123456789ABCDEFxfb";
	public static String STRING_DELIMITERS = "\"'";

	/**
	 * The constructor.
	 */
	public RefactorHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		String project = null;
		IEditorPart editorPart = window.getActivePage().getActiveEditor();

		if (editorPart != null) {
			IFileEditorInput input = (IFileEditorInput) editorPart
					.getEditorInput();
			IFile file = input.getFile();
			IProject activeProject = file.getProject();
			project = activeProject.getLocation().toString();
		}
		if (project == null)
			return null;
		ITextEditor texteditor = (ITextEditor) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.getEditorReferences()[0].getEditor(true);
		IDocumentProvider dp = texteditor.getDocumentProvider();
		int offset = 0;
		IEditorPart editor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (editor instanceof ITextEditor) {
			ISelectionProvider selectionProvider = ((ITextEditor) editor)
					.getSelectionProvider();
			ISelection selection = selectionProvider.getSelection();
			if (selection instanceof ITextSelection) {
				ITextSelection textSelection = (ITextSelection) selection;
				offset = textSelection.getOffset();
			}
		}
		IWorkbenchPart workbenchPart = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActivePart();
		workbenchPart.getSite().getPage().getActiveEditor().doSave(null);
		IFile file = (IFile) workbenchPart.getSite().getPage()
				.getActiveEditor().getEditorInput().getAdapter(IFile.class);
		String content = null;
		try {
			Scanner sc = new Scanner(file.getContents());
			sc.useDelimiter("\\A");
			content = sc.next();
			sc.close();
		} catch (CoreException e) {
			return null;
		}
		String[] lines = content.split(System.getProperty("line.separator"));
		int currentOffset = 0;
		String editLine = null;
		int newOffset = 0;
		int lineNumber = 0;
		for (String line : lines) {
			currentOffset += line.length() + 1;
			if (currentOffset > offset) {
				editLine = line;
				newOffset = offset - currentOffset + line.length() + 1;
				break;
			}
			lineNumber++;
		}
		String lineLeft = editLine.substring(0, newOffset);
		lineLeft = lineLeft.replace("\\\\", "");
		lineLeft = lineLeft.replace("\\\"", "");

		String leftPart = null;
		String variable = null;
		String rightPart = null;

		char delimiter = '"';
		boolean opened = false;
		int counter = 0;
		int left = 0;
		int right = 0;
		int j = 0;
		for (char c : lineLeft.toCharArray()) {
			if (opened) {
				if (c == delimiter) {
					if (counter % 2 == 0)
						opened = false;
					counter = 0;
				} else if (c == '\\') {
					counter++;
				} else {
					counter = 0;
				}
			} else {
				if (STRING_DELIMITERS.contains("" + c)) {
					delimiter = c;
					opened = true;
					left = j;
				}
			}
			j++;
		}

		if (opened) {
			// string
			counter = 0;
			for (right = newOffset + 1; right < editLine.length(); right++) {
				char current = editLine.charAt(right - 1);
				if (current == '\\') {
					counter++;
				} else if (current == delimiter) {
					if (counter % 2 == 0)
						break;
					else
						counter = 0;
				} else {
					counter = 0;
				}
			}
			leftPart = editLine.substring(0, left);
			variable = editLine.substring(left, right);
			rightPart = editLine.substring(right);
		} else {
			// number
			left = 0;
			for (left = newOffset; left > 0; left--) {
				if (!VALID_NUMBER.contains("" + editLine.charAt(left - 1))) {
					break;
				}
			}
			for (right = newOffset; right < editLine.length(); right++) {
				if (!VALID_NUMBER.contains("" + editLine.charAt(right))) {
					break;
				}
			}
			leftPart = editLine.substring(0, left);
			variable = editLine.substring(left, right);
			rightPart = editLine.substring(right);
		}
		if (variable.length() == 0)
			return null;

		InputDialog dialog = new InputDialog(window.getShell(),
				"Variable name", "Please enter a name for the variable:", "",
				new IInputValidator() {

					public String isValid(String newText) {
						Pattern p = Pattern.compile("[^a-zA-Z0-9_]");
						if (p.matcher(newText).find()) {
							return "The variable name can only contain letters, numbers and underscores.";
						} else {
							return null;
						}
					}
				});
		if (dialog.open() == Window.CANCEL)
			return null;
		String variableName = dialog.getValue();

		lines[lineNumber] = leftPart + "constants." + variableName + rightPart;
		try {
			BufferedWriter b = new BufferedWriter(new FileWriter(file
					.getLocation().toString(), false));
			for (String line : lines)
				b.write(line + "\n");
			b.close();
		} catch (IOException e) {
		}
		try {
			dp.resetDocument(texteditor.getEditorInput());
		} catch (CoreException e) {
		}

		Scanner sc;
		try {
			sc = new Scanner(new FileInputStream(project + "/constants.py"));
			sc.useDelimiter("\\A");
			content = sc.next();
			sc.close();
		} catch (FileNotFoundException e1) {
			System.out.println(getClass());
			sc = new Scanner(getClass().getResourceAsStream(
					"../../../../../../resources/templates/constants.py"));
			sc.useDelimiter("\\A");
			content = sc.next();
			sc.close();
		}

		String section = null;
		if (content.toLowerCase().contains(
				"# endsection " + variableName.toLowerCase().split("_")[0])) {
			section = variableName.toLowerCase().split("_")[0];
		} else if (content.toLowerCase().contains("# endsection other")) {
			section = "other";
		}

		String newContent = "";
		if (section == null) {
			content += "\n" + variableName + " = " + variable;
			newContent = content;
		} else {
			for (String line : content.split("\n")) {
				if (line.contains("# endsection " + section)) {
					newContent += variableName + " = " + variable + "\n";
				}
				newContent += line + "\n";
			}
		}

		try {
			FileWriter w = new FileWriter(project + "/constants.py", false);
			w.write(newContent);
			w.close();
		} catch (IOException e) {
		}
		return null;
	}
}
