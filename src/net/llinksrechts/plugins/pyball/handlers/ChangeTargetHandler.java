package net.llinksrechts.plugins.pyball.handlers;

import net.llinksrechts.plugins.pyball.communication.Uploader;
import net.llinksrechts.plugins.pyball.dialogs.DeviceSelectionDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class ChangeTargetHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		String target = new DeviceSelectionDialog(window.getShell())
				.openDialog();
		if (target == null)
			return null;
		Uploader.target = target;
		return null;
	}

}
