package net.llinksrechts.plugins.pyball.handlers;

import net.llinksrechts.plugins.pyball.communication.Uploader;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

public class ChangeKeyHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Uploader.ensureKeyExists(true);
		return null;
	}
}
