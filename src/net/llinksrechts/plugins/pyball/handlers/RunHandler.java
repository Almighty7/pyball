package net.llinksrechts.plugins.pyball.handlers;

import net.llinksrechts.plugins.pyball.communication.Uploader;
import net.llinksrechts.plugins.pyball.dialogs.DeviceSelectionDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.handlers.HandlerUtil;

import com.jcraft.jsch.JSchException;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class RunHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public RunHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		if (Uploader.target == null) {
			IWorkbenchWindow window = HandlerUtil
					.getActiveWorkbenchWindowChecked(event);
			String target = new DeviceSelectionDialog(window.getShell())
					.openDialog();
			if (target == null)
				return null;
			Uploader.target = target;
		}
		final RunHandler handler = this;
		final String projectPath = Uploader.getProjectPath();
		if (!Uploader.ensureKeyExists()) {
			return null;
		}
		final MessageConsole console = Uploader.findConsole("Botball output");
		Thread t = new Thread(new Runnable() {

			public void run() {
				try {
					try {
						Uploader.run(handler, projectPath, console);
					} catch (JSchException e) {
						console.newMessageStream().write(
								"Could not connect to the bot");
					} catch (Exception e) {
						console.newMessageStream().write("An error occured");
					}
				} catch (Exception e) {
				}
			}
		});
		t.start();

		return null;
	}
}
