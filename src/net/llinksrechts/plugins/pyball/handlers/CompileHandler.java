package net.llinksrechts.plugins.pyball.handlers;

import net.llinksrechts.plugins.pyball.communication.Uploader;
import net.llinksrechts.plugins.pyball.dialogs.DeviceSelectionDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;

import com.jcraft.jsch.JSchException;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class CompileHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public CompileHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		if (Uploader.target == null) {
			IWorkbenchWindow window = HandlerUtil
					.getActiveWorkbenchWindowChecked(event);
			String target = new DeviceSelectionDialog(window.getShell())
					.openDialog();
			if (target == null)
				return null;
			Uploader.target = target;
		}
		final CompileHandler handler = this;
		final String projectPath = Uploader.getProjectPath();
		if (!Uploader.ensureKeyExists()) {
			return null;
		}
		final MessageConsoleStream consoleStream = Uploader.findConsole("Botball output").newMessageStream();
		Thread t = new Thread(new Runnable() {

			public void run() {
				try {
					try {
						Uploader.compile(handler, projectPath, consoleStream);
					} catch (JSchException e) {
						consoleStream.write("Could not connect to the bot");
					} catch (Exception e) {
						consoleStream.write("An error occured");
					}
				} catch (Exception e) {
				}
			}
		});
		t.start();

		return null;
	}
}
