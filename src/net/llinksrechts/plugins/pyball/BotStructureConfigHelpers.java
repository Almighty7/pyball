package net.llinksrechts.plugins.pyball;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.python.pydev.plugin.PyStructureConfigHelpers;
import org.python.pydev.plugin.nature.PythonNature;
import org.python.pydev.shared_core.callbacks.ICallback;

public class BotStructureConfigHelpers extends PyStructureConfigHelpers {
	public static void createBotballProject(
			IProjectDescription description,
			IProject projectHandle,
			IProgressMonitor monitor,
			String projectType,
			String projectInterpreter,
			ICallback<List<IContainer>, IProject> getSourceFolderHandlesCallback,
			ICallback<List<String>, IProject> getExternalSourceFolderHandlesCallback,
			ICallback<List<IPath>, IProject> getExistingSourceFolderHandlesCallback,
			ICallback<Map<String, String>, IProject> getVariableSubstitutionCallback)
			throws CoreException, OperationCanceledException {
		PyStructureConfigHelpers.createPydevProject(description, projectHandle,
				monitor, projectType, projectInterpreter,
				getSourceFolderHandlesCallback,
				getExternalSourceFolderHandlesCallback,
				getExistingSourceFolderHandlesCallback);

		String home = System.getProperty("user.home");
		String projectFileAppend = "<pydev_pathproperty name=\"org.python.pydev.PROJECT_EXTERNAL_SOURCE_PATH\">"
				+ "<path>" + home + "/.pyball"
				+ "</path>" + "</pydev_pathproperty>" + "</pydev_project>";
		Path path = Paths.get(projectHandle.getLocation().toString()
				+ "/.pydevproject");
		Charset charset = StandardCharsets.UTF_8;

		String content;
		try {
			content = new String(Files.readAllBytes(path), charset);
			content = content.replaceAll("</pydev_project>", projectFileAppend);
			Files.write(path, content.getBytes(charset));
		} catch (IOException e) {
		}
		PythonNature.addNature(projectHandle, null, projectType, null, null,
				projectInterpreter, null);
	}
}
