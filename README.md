PyBall
===
PyBall is an eclipse plugin for uploading and executing python code on the wallaby
[Latest Build](https://lukor.tk/files/wallapy/net.llinksrechts.plugins.pyball_1.1.1.b.jar)
[Other Repositories](https://gitlab.com/groups/Almighty7)
